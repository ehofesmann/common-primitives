import unittest

from d3m import container
from d3m.metadata import base

from common_primitives import utils


class TestUtils(unittest.TestCase):
    def test_copy_elements_metadata(self):
        metadata = base.Metadata()

        metadata = metadata.update((), {'level0': 'foobar0'})

        metadata = metadata.update(('level1',), {'level1': 'foobar1'})

        metadata = metadata.update((base.ALL_ELEMENTS,), {'level1a': 'foobar1a', 'level1b': 'foobar1b'})

        metadata = metadata.update(('level1',), {'level1b': base.NO_VALUE})

        metadata = metadata.update(('level1', 'level2'), {'level2': 'foobar2'})

        metadata = metadata.update((base.ALL_ELEMENTS, base.ALL_ELEMENTS), {'level2a': 'foobar2a', 'level2b': 'foobar2b'})

        metadata = metadata.update(('level1', 'level2'), {'level2b': base.NO_VALUE})

        metadata = metadata.update(('level1', 'level2', 'level3'), {'level3': 'foobar3'})

        metadata = metadata.update((base.ALL_ELEMENTS, base.ALL_ELEMENTS, 'level3'), {'level3a': 'foobar3a'})

        metadata = metadata.update(('level1', 'level2', 'level3.1'), {'level3.1': 'foobar3.1'})

        metadata = metadata.update(('level1', 'level2', 'level3', 'level4'), {'level4': 'foobar4'})

        metadata = metadata.update(('level1', 'level2', 'level3', 'level4.1'), {'level4.1': 'foobar4.1'})

        self.assertTrue(metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {'level0': 'foobar0'},
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {'level1a': 'foobar1a', 'level1b': 'foobar1b'},
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {'level2a': 'foobar2a', 'level2b': 'foobar2b'},
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__', 'level3'],
            'metadata': {'level3a': 'foobar3a'},
        }, {
            'selector': ['level1'],
            'metadata': {'level1': 'foobar1', 'level1b': '__NO_VALUE__'},
        }, {
            'selector': ['level1', 'level2'],
            'metadata': {'level2': 'foobar2', 'level2b': '__NO_VALUE__'},
        }, {
            'selector': ['level1', 'level2', 'level3'],
            'metadata': {'level3': 'foobar3'},
        }, {
            'selector': ['level1', 'level2', 'level3', 'level4'],
            'metadata': {'level4': 'foobar4'},
        }, {
            'selector': ['level1', 'level2', 'level3', 'level4.1'],
            'metadata': {'level4.1': 'foobar4.1'},
        }, {
            'selector': ['level1', 'level2', 'level3.1'],
            'metadata': {'level3.1': 'foobar3.1'},
        }])

        self.assertEqual(metadata.query(('level1', 'level2')), {
            'level2a': 'foobar2a',
            'level2': 'foobar2',
        })

        target_metadata = base.Metadata()

        target_metadata = target_metadata.update((), {'level0z': 'foobar0z'})

        target_metadata = utils.copy_elements_metadata(metadata, target_metadata, ())

        self.assertTrue(target_metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {'level0z': 'foobar0z'},
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {'level1a': 'foobar1a', 'level1b': 'foobar1b'},
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {'level2a': 'foobar2a', 'level2b': 'foobar2b'},
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__', 'level3'],
            'metadata': {'level3a': 'foobar3a'},
        }, {
            'selector': ['level1'],
            'metadata': {'level1': 'foobar1', 'level1b': '__NO_VALUE__'},
        }, {
            'selector': ['level1', 'level2'],
            'metadata': {'level2': 'foobar2', 'level2b': '__NO_VALUE__'},
        }, {
            'selector': ['level1', 'level2', 'level3'],
            'metadata': {'level3': 'foobar3'},
        }, {
            'selector': ['level1', 'level2', 'level3', 'level4'],
            'metadata': {'level4': 'foobar4'},
        }, {
            'selector': ['level1', 'level2', 'level3', 'level4.1'],
            'metadata': {'level4.1': 'foobar4.1'},
        }, {
            'selector': ['level1', 'level2', 'level3.1'],
            'metadata': {'level3.1': 'foobar3.1'},
        }])

        target_metadata = base.Metadata()

        target_metadata = target_metadata.update((), {'level0z': 'foobar0z'})

        target_metadata = utils.copy_elements_metadata(metadata, target_metadata, ('level1',))

        self.assertEqual(target_metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {'level0z': 'foobar0z'},
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {'level2a': 'foobar2a', 'level2b': 'foobar2b'},
        }, {
            'selector': ['__ALL_ELEMENTS__', 'level3'],
            'metadata': {'level3a': 'foobar3a'},
        }, {
            'selector': ['level2'],
            'metadata': {'level2': 'foobar2', 'level2b': '__NO_VALUE__'},
        }, {
            'selector': ['level2', 'level3'],
            'metadata': {'level3': 'foobar3'},
        }, {
            'selector': ['level2', 'level3', 'level4'],
            'metadata': {'level4': 'foobar4'},
        }, {
            'selector': ['level2', 'level3', 'level4.1'],
            'metadata': {'level4.1': 'foobar4.1'},
        }, {
            'selector': ['level2', 'level3.1'],
            'metadata': {'level3.1': 'foobar3.1'},
        }])

        self.assertEqual(target_metadata.query(('level2',)), {
            'level2a': 'foobar2a',
            'level2': 'foobar2',
        })

        target_metadata = base.Metadata()

        target_metadata = target_metadata.update((), {'level0z': 'foobar0z'})
        target_metadata = target_metadata.update(('zlevel',), {'level1z': 'foobar1z'})

        target_metadata = utils.copy_elements_metadata(metadata, target_metadata, ('level1',), ('zlevel',))

        self.assertEqual(target_metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {'level0z': 'foobar0z'},
        }, {
            'selector': ['zlevel'],
            'metadata': {'level1z': 'foobar1z'},
        }, {
            'selector': ['zlevel', '__ALL_ELEMENTS__'],
            'metadata': {'level2a': 'foobar2a', 'level2b': 'foobar2b'},
        }, {
            'selector': ['zlevel', '__ALL_ELEMENTS__', 'level3'],
            'metadata': {'level3a': 'foobar3a'},
        }, {
            'selector': ['zlevel', 'level2'],
            'metadata': {'level2': 'foobar2', 'level2b': '__NO_VALUE__'},
        }, {
            'selector': ['zlevel', 'level2', 'level3'],
            'metadata': {'level3': 'foobar3'},
        }, {
            'selector': ['zlevel', 'level2', 'level3', 'level4'],
            'metadata': {'level4': 'foobar4'},
        }, {
            'selector': ['zlevel', 'level2', 'level3', 'level4.1'],
            'metadata': {'level4.1': 'foobar4.1'},
        }, {
            'selector': ['zlevel', 'level2', 'level3.1'],
            'metadata': {'level3.1': 'foobar3.1'},
        }])

        self.assertEqual(target_metadata.query(('zlevel', 'level2',)), {
            'level2a': 'foobar2a',
            'level2': 'foobar2',
        })

        target_metadata = base.Metadata()

        target_metadata = target_metadata.update((), {'level0z': 'foobar0z'})

        target_metadata = utils.copy_elements_metadata(metadata, target_metadata, ('level1', 'level2'))

        self.assertEqual(target_metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {'level0z': 'foobar0z'},
        }, {
            'selector': ['level3'],
            'metadata': {'level3': 'foobar3', 'level3a': 'foobar3a'},
        }, {
            'selector': ['level3', 'level4'],
            'metadata': {'level4': 'foobar4'},
        }, {
            'selector': ['level3', 'level4.1'],
            'metadata': {'level4.1': 'foobar4.1'},
        }, {
            'selector': ['level3.1'],
            'metadata': {'level3.1': 'foobar3.1'},
        }])

        target_metadata = base.Metadata()

        target_metadata = target_metadata.update((), {'level0z': 'foobar0z'})
        target_metadata = target_metadata.update(('zlevel',), {'level1z': 'foobar1z'})

        target_metadata = utils.copy_elements_metadata(metadata, target_metadata, ('level1', 'level2'), ('zlevel',))

        self.assertEqual(target_metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {'level0z': 'foobar0z'},
        }, {
            'selector': ['zlevel'],
            'metadata': {'level1z': 'foobar1z'},
        }, {
            'selector': ['zlevel', 'level3'],
            'metadata': {'level3': 'foobar3', 'level3a': 'foobar3a'},
        }, {
            'selector': ['zlevel', 'level3', 'level4'],
            'metadata': {'level4': 'foobar4'},
        }, {
            'selector': ['zlevel', 'level3', 'level4.1'],
            'metadata': {'level4.1': 'foobar4.1'},
        }, {
            'selector': ['zlevel', 'level3.1'],
            'metadata': {'level3.1': 'foobar3.1'},
        }])

    def test_copy_metadata(self):
        metadata = base.Metadata()

        metadata = metadata.update((), {'level0': 'foobar0'})

        metadata = metadata.update(('level1',), {'level1': 'foobar1'})

        metadata = metadata.update((base.ALL_ELEMENTS,), {'level1a': 'foobar1a', 'level1b': 'foobar1b'})

        metadata = metadata.update(('level1',), {'level1b': base.NO_VALUE})

        metadata = metadata.update(('level1', 'level2'), {'level2': 'foobar2'})

        metadata = metadata.update((base.ALL_ELEMENTS, base.ALL_ELEMENTS), {'level2a': 'foobar2a', 'level2b': 'foobar2b'})

        metadata = metadata.update(('level1', 'level2'), {'level2b': base.NO_VALUE})

        metadata = metadata.update(('level1', 'level2', 'level3'), {'level3': 'foobar3'})

        metadata = metadata.update((base.ALL_ELEMENTS, base.ALL_ELEMENTS, 'level3'), {'level3a': 'foobar3a'})

        metadata = metadata.update(('level1', 'level2', 'level3.1'), {'level3.1': 'foobar3.1'})

        metadata = metadata.update(('level1', 'level2', 'level3', 'level4'), {'level4': 'foobar4'})

        metadata = metadata.update(('level1', 'level2', 'level3', 'level4.1'), {'level4.1': 'foobar4.1'})

        self.assertTrue(metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {'level0': 'foobar0'},
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {'level1a': 'foobar1a', 'level1b': 'foobar1b'},
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {'level2a': 'foobar2a', 'level2b': 'foobar2b'},
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__', 'level3'],
            'metadata': {'level3a': 'foobar3a'},
        }, {
            'selector': ['level1'],
            'metadata': {'level1': 'foobar1', 'level1b': '__NO_VALUE__'},
        }, {
            'selector': ['level1', 'level2'],
            'metadata': {'level2': 'foobar2', 'level2b': '__NO_VALUE__'},
        }, {
            'selector': ['level1', 'level2', 'level3'],
            'metadata': {'level3': 'foobar3'},
        }, {
            'selector': ['level1', 'level2', 'level3', 'level4'],
            'metadata': {'level4': 'foobar4'},
        }, {
            'selector': ['level1', 'level2', 'level3', 'level4.1'],
            'metadata': {'level4.1': 'foobar4.1'},
        }, {
            'selector': ['level1', 'level2', 'level3.1'],
            'metadata': {'level3.1': 'foobar3.1'},
        }])

        self.assertEqual(metadata.query(('level1', 'level2')), {
            'level2a': 'foobar2a',
            'level2': 'foobar2',
        })

        target_metadata = base.Metadata()

        target_metadata = target_metadata.update((), {'level0z': 'foobar0z'})

        target_metadata = utils.copy_metadata(metadata, target_metadata, ())

        self.assertTrue(target_metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {'level0': 'foobar0', 'level0z': 'foobar0z'},
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {'level1a': 'foobar1a', 'level1b': 'foobar1b'},
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {'level2a': 'foobar2a', 'level2b': 'foobar2b'},
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__', 'level3'],
            'metadata': {'level3a': 'foobar3a'},
        }, {
            'selector': ['level1'],
            'metadata': {'level1': 'foobar1', 'level1b': '__NO_VALUE__'},
        }, {
            'selector': ['level1', 'level2'],
            'metadata': {'level2': 'foobar2', 'level2b': '__NO_VALUE__'},
        }, {
            'selector': ['level1', 'level2', 'level3'],
            'metadata': {'level3': 'foobar3'},
        }, {
            'selector': ['level1', 'level2', 'level3', 'level4'],
            'metadata': {'level4': 'foobar4'},
        }, {
            'selector': ['level1', 'level2', 'level3', 'level4.1'],
            'metadata': {'level4.1': 'foobar4.1'},
        }, {
            'selector': ['level1', 'level2', 'level3.1'],
            'metadata': {'level3.1': 'foobar3.1'},
        }])

        target_metadata = base.Metadata()

        target_metadata = target_metadata.update((), {'level0z': 'foobar0z'})

        target_metadata = utils.copy_metadata(metadata, target_metadata, ('level1',))

        self.assertEqual(target_metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {'level0z': 'foobar0z', 'level1': 'foobar1', 'level1b': '__NO_VALUE__', 'level1a': 'foobar1a'},
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {'level2a': 'foobar2a', 'level2b': 'foobar2b'},
        }, {
            'selector': ['__ALL_ELEMENTS__', 'level3'],
            'metadata': {'level3a': 'foobar3a'},
        }, {
            'selector': ['level2'],
            'metadata': {'level2': 'foobar2', 'level2b': '__NO_VALUE__'},
        }, {
            'selector': ['level2', 'level3'],
            'metadata': {'level3': 'foobar3'},
        }, {
            'selector': ['level2', 'level3', 'level4'],
            'metadata': {'level4': 'foobar4'},
        }, {
            'selector': ['level2', 'level3', 'level4.1'],
            'metadata': {'level4.1': 'foobar4.1'},
        }, {
            'selector': ['level2', 'level3.1'],
            'metadata': {'level3.1': 'foobar3.1'},
        }])

        self.assertEqual(target_metadata.query(('level2',)), {
            'level2a': 'foobar2a',
            'level2': 'foobar2',
        })

        target_metadata = base.Metadata()

        target_metadata = target_metadata.update((), {'level0z': 'foobar0z'})
        target_metadata = target_metadata.update(('zlevel',), {'level1z': 'foobar1z'})

        target_metadata = utils.copy_metadata(metadata, target_metadata, ('level1',), ('zlevel',))

        self.assertEqual(target_metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {'level0z': 'foobar0z'},
        }, {
            'selector': ['zlevel'],
            'metadata': {'level1z': 'foobar1z', 'level1': 'foobar1', 'level1b': '__NO_VALUE__', 'level1a': 'foobar1a'},
        }, {
            'selector': ['zlevel', '__ALL_ELEMENTS__'],
            'metadata': {'level2a': 'foobar2a', 'level2b': 'foobar2b'},
        }, {
            'selector': ['zlevel', '__ALL_ELEMENTS__', 'level3'],
            'metadata': {'level3a': 'foobar3a'},
        }, {
            'selector': ['zlevel', 'level2'],
            'metadata': {'level2': 'foobar2', 'level2b': '__NO_VALUE__'},
        }, {
            'selector': ['zlevel', 'level2', 'level3'],
            'metadata': {'level3': 'foobar3'},
        }, {
            'selector': ['zlevel', 'level2', 'level3', 'level4'],
            'metadata': {'level4': 'foobar4'},
        }, {
            'selector': ['zlevel', 'level2', 'level3', 'level4.1'],
            'metadata': {'level4.1': 'foobar4.1'},
        }, {
            'selector': ['zlevel', 'level2', 'level3.1'],
            'metadata': {'level3.1': 'foobar3.1'},
        }])

        self.assertEqual(target_metadata.query(('zlevel', 'level2',)), {
            'level2a': 'foobar2a',
            'level2': 'foobar2',
        })

        target_metadata = base.Metadata()

        target_metadata = target_metadata.update((), {'level0z': 'foobar0z'})

        target_metadata = utils.copy_metadata(metadata, target_metadata, ('level1', 'level2'))

        self.assertEqual(target_metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {'level0z': 'foobar0z', 'level2': 'foobar2', 'level2b': '__NO_VALUE__', 'level2a': 'foobar2a'},
        }, {
            'selector': ['level3'],
            'metadata': {'level3': 'foobar3', 'level3a': 'foobar3a'},
        }, {
            'selector': ['level3', 'level4'],
            'metadata': {'level4': 'foobar4'},
        }, {
            'selector': ['level3', 'level4.1'],
            'metadata': {'level4.1': 'foobar4.1'},
        }, {
            'selector': ['level3.1'],
            'metadata': {'level3.1': 'foobar3.1'},
        }])

        target_metadata = base.Metadata()

        target_metadata = target_metadata.update((), {'level0z': 'foobar0z'})
        target_metadata = target_metadata.update(('zlevel',), {'level1z': 'foobar1z'})

        target_metadata = utils.copy_metadata(metadata, target_metadata, ('level1', 'level2'), ('zlevel',))

        self.assertEqual(target_metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {'level0z': 'foobar0z'},
        }, {
            'selector': ['zlevel'],
            'metadata': {'level1z': 'foobar1z', 'level2': 'foobar2', 'level2b': '__NO_VALUE__', 'level2a': 'foobar2a'},
        }, {
            'selector': ['zlevel', 'level3'],
            'metadata': {'level3': 'foobar3', 'level3a': 'foobar3a'},
        }, {
            'selector': ['zlevel', 'level3', 'level4'],
            'metadata': {'level4': 'foobar4'},
        }, {
            'selector': ['zlevel', 'level3', 'level4.1'],
            'metadata': {'level4.1': 'foobar4.1'},
        }, {
            'selector': ['zlevel', 'level3.1'],
            'metadata': {'level3.1': 'foobar3.1'},
        }])

    def test_select_columns(self):
        data = container.DataFrame({'a': [1, 2, 3], 'b': [4, 5, 6], 'c': [7, 8, 9]})
        data.metadata = data.metadata.update_column(0, {'name': 'aaa'})
        data.metadata = data.metadata.update_column(1, {'name': 'bbb'})
        data.metadata = data.metadata.update_column(2, {'name': 'ccc'})

        data_metadata_before = data.metadata.to_json_structure()

        selected = utils.select_columns(data, [1, 0, 2, 1])

        self.assertIs(selected, selected.metadata.for_value)

        self.assertEqual(selected.values.tolist(), [[4, 1, 7, 4], [5, 2, 8, 5], [6, 3, 9, 6]])

        self.assertEqual(selected.metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'schema': base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.pandas.DataFrame',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/Table'],
                'dimension': {
                    'name': 'rows',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                    'length': 3,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {
                'dimension': {
                    'name': 'columns',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                    'length': 4,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {'structural_type': 'numpy.int64'},
        }, {
            'selector': ['__ALL_ELEMENTS__', 0],
            'metadata': {'name': 'bbb'},
        }, {
            'selector': ['__ALL_ELEMENTS__', 1],
            'metadata': {'name': 'aaa'},
        }, {
            'selector': ['__ALL_ELEMENTS__', 2],
            'metadata': {'name': 'ccc'},
        }, {
            'selector': ['__ALL_ELEMENTS__', 3],
            'metadata': {'name': 'bbb'},
        }])

        self.assertEqual(data.metadata.to_json_structure(), data_metadata_before)

    def test_append_columns(self):
        left = container.DataFrame({'a1': [1, 2, 3], 'b1': [4, 5, 6], 'c1': [7, 8, 9]}, {
            'top_level': 'left',
        })
        left.metadata = left.metadata.update_column(0, {'name': 'aaa111'})
        left.metadata = left.metadata.update_column(1, {'name': 'bbb111'})
        left.metadata = left.metadata.update_column(2, {'name': 'ccc111'})

        right = container.DataFrame({'a2': [11, 12, 13], 'b2': [14, 15, 16], 'c2': [17, 18, 19]}, {
            'top_level': 'right',
        })
        right.metadata = right.metadata.update_column(0, {'name': 'aaa222'})
        right.metadata = right.metadata.update_column(1, {'name': 'bbb222'})
        right.metadata = right.metadata.update_column(2, {'name': 'ccc222'})

        right_metadata_before = right.metadata.to_json_structure()

        data = utils.append_columns(left, right, use_right_metadata=False)

        self.assertIs(data, data.metadata.for_value)

        self.assertEqual(data.values.tolist(), [[1, 4, 7, 11, 14, 17], [2, 5, 8, 12, 15, 18], [3, 6, 9, 13, 16, 19]])

        self.assertEqual(data.metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'top_level': 'left',
                'schema': base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.pandas.DataFrame',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/Table'],
                'dimension': {
                    'name': 'rows',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                    'length': 3,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {
                'dimension': {
                    'name': 'columns',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                    'length': 6,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {
                'structural_type': 'numpy.int64',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 0],
            'metadata': {'name': 'aaa111'},
        }, {
            'selector': ['__ALL_ELEMENTS__', 1],
            'metadata': {'name': 'bbb111'},
        }, {
            'selector': ['__ALL_ELEMENTS__', 2],
            'metadata': {'name': 'ccc111'},
        }, {
            'selector': ['__ALL_ELEMENTS__', 3],
            'metadata': {
                'structural_type': 'numpy.int64',
                'name': 'aaa222',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 4],
            'metadata': {
                'structural_type': 'numpy.int64',
                'name': 'bbb222',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 5],
            'metadata': {
                'structural_type': 'numpy.int64',
                'name': 'ccc222',
            },
        }])

        data = utils.append_columns(left, right, use_right_metadata=True)

        self.assertIs(data, data.metadata.for_value)

        self.assertEqual(data.values.tolist(), [[1, 4, 7, 11, 14, 17], [2, 5, 8, 12, 15, 18], [3, 6, 9, 13, 16, 19]])

        self.assertEqual(data.metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'top_level': 'right',
                'schema': base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.pandas.DataFrame',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/Table'],
                'dimension': {
                    'name': 'rows',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                    'length': 3,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {
                'dimension': {
                    'name': 'columns',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                    'length': 6,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {
                'structural_type': 'numpy.int64',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 5],
            'metadata': {'name': 'ccc222'},
        }, {
            'selector': ['__ALL_ELEMENTS__', 4],
            'metadata': {'name': 'bbb222'},
        }, {
            'selector': ['__ALL_ELEMENTS__', 3],
            'metadata': {'name': 'aaa222'},
        }, {
            'selector': ['__ALL_ELEMENTS__', 0],
            'metadata': {
                'structural_type': 'numpy.int64',
                'name': 'aaa111',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 1],
            'metadata': {
                'structural_type': 'numpy.int64',
                'name': 'bbb111',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 2],
            'metadata': {
                'structural_type': 'numpy.int64',
                'name': 'ccc111',
            },
        }])

        self.assertEqual(right.metadata.to_json_structure(), right_metadata_before)

    def test_replace_columns(self):
        main = container.DataFrame({'a1': [1, 2, 3], 'b1': [4, 5, 6], 'c1': [7, 8, 9]}, {
            'top_level': 'main',
        })
        main.metadata = main.metadata.update_column(0, {'name': 'aaa111'})
        main.metadata = main.metadata.update_column(1, {'name': 'bbb111', 'extra': 'should_be_removed'})
        main.metadata = main.metadata.update_column(2, {'name': 'ccc111'})

        columns = container.DataFrame({'a2': [11, 12, 13], 'b2': [14, 15, 16]}, {
            'top_level': 'columns',
        })
        columns.metadata = columns.metadata.update_column(0, {'name': 'aaa222'})
        columns.metadata = columns.metadata.update_column(1, {'name': 'bbb222'})

        new_main = utils.replace_columns(main, columns, [1, 2])

        self.assertIs(new_main, new_main.metadata.for_value)

        self.assertEqual(new_main.values.tolist(), [[1, 11, 14], [2, 12, 15], [3, 13, 16]])

        self.assertEqual(new_main.metadata.to_json_structure(), [{
            'selector': [], 'metadata': {
                'top_level': 'main',
                'schema': base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.pandas.DataFrame',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/Table'],
                'dimension': {
                    'name': 'rows',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                    'length': 3,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {
                'dimension': {
                    'name': 'columns',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                    'length': 3,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {
                'structural_type': 'numpy.int64',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 0],
            'metadata': {
                'name': 'aaa111',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 1],
            'metadata': {
                'structural_type': 'numpy.int64',
                'name': 'aaa222',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 2],
            'metadata': {
                'structural_type': 'numpy.int64',
                'name': 'bbb222',
            },
        }])


if __name__ == '__main__':
    unittest.main()
