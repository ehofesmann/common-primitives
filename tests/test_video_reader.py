import unittest
import os
import numpy as np

from common_primitives.video_reader import VideoReader, Hyperparams
from d3m.container.dataset import D3MDatasetLoader
from common_primitives import dataset_to_dataframe


class VideoReaderTestCase(unittest.TestCase):
    def test_produce(self):
        vr = VideoReader(hyperparams=Hyperparams.defaults())
        dataset_doc_path = os.path.join(os.path.realpath('.'), 'data/video_dataset_1/datasetDoc.json')
        dataset = D3MDatasetLoader().load('file://'+dataset_doc_path)
        
        df0 = dataset_to_dataframe.DatasetToDataFramePrimitive(hyperparams=dataset_to_dataframe.Hyperparams(dataframe_resource='0')).produce(inputs=dataset).value
        
        data_run = vr.produce(inputs=df0).value
        
        data_run = np.array(data_run)
        assert((data_run.shape[0] == 2) and (data_run[0][0].shape[1:] == (240,320,3)) and (data_run[1][0].shape[1:] == (240,320,3)))

    def test_resize(self):
        vr = VideoReader(hyperparams=Hyperparams(Hyperparams.defaults(), resize_to=(224,224)))

        dataset_doc_path = os.path.join(os.path.realpath('.'), 'data/video_dataset_1/datasetDoc.json')
        dataset = D3MDatasetLoader().load('file://'+dataset_doc_path)
        
        df0 = dataset_to_dataframe.DatasetToDataFramePrimitive(hyperparams=dataset_to_dataframe.Hyperparams(dataframe_resource='0')).produce(inputs=dataset).value
        
        data_run = vr.produce(inputs=df0).value
 
        data_run = np.array(data_run)
        assert((data_run.shape[0] == 2) and (data_run[0][0].shape[1:] == (224,224,3)) and (data_run[1][0].shape[1:] == (224,224,3)))

if __name__ == '__main__':
    unittest.main()
