import unittest

import numpy

from d3m import container
from d3m.metadata import base as metadata_base

from common_primitives import stack_ndarray_column


class StackNDArrayColumnPrimitiveTestCase(unittest.TestCase):
    def _get_data(self):
        data = container.DataFrame({
            'a': [1, 2, 3],
            'b': [container.ndarray([2, 3, 4]), container.ndarray([5, 6, 7]), container.ndarray([8, 9, 10])]
        }, {
            'top_level': 'foobar1',
        })

        # Patch up invalid metadata.
        # See: https://gitlab.com/datadrivendiscovery/d3m/issues/145
        # TODO: Remove once fixed in the d3m core package.
        data.metadata = data.metadata.remove((metadata_base.ALL_ELEMENTS, metadata_base.ALL_ELEMENTS, metadata_base.ALL_ELEMENTS))
        data.metadata = data.metadata.update((metadata_base.ALL_ELEMENTS, 1, metadata_base.ALL_ELEMENTS), {
            'structural_type': numpy.int64,
        })

        return data

    def test_basic(self):
        data = self._get_data()

        data_metadata_before = data.metadata.to_json_structure()

        stack_hyperparams_class = stack_ndarray_column.StackNDArrayColumnPrimitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']
        stack_primitive = stack_ndarray_column.StackNDArrayColumnPrimitive(hyperparams=stack_hyperparams_class.defaults())
        stack_array = stack_primitive.produce(inputs=data).value

        self._test_metadata(stack_array.metadata, False)

        self.assertEqual(data.metadata.to_json_structure(), data_metadata_before)

    def test_can_accept(self):
        data = self._get_data()

        data_metadata_before = data.metadata.to_json_structure()

        data_metadata = data.metadata.set_for_value(None)

        stack_hyperparams_class = stack_ndarray_column.StackNDArrayColumnPrimitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']
        stack_array_metadata = stack_ndarray_column.StackNDArrayColumnPrimitive.can_accept(method_name='produce', arguments={'inputs': data_metadata}, hyperparams=stack_hyperparams_class.defaults())

        self._test_metadata(stack_array_metadata, True)

        self.assertEqual(data.metadata.to_json_structure(), data_metadata_before)

    def _test_metadata(self, metadata, rows_no_value):
        rows_metadata = {
            'dimension': {
                'length': 3,
                'name': 'columns',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
            },
            'name': 'b',
        }

        if rows_no_value:
            rows_metadata['structural_type'] = '__NO_VALUE__'

        self.assertEqual(metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'top_level': 'foobar1',
                'schema': metadata_base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.numpy.ndarray',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/Table'],
                'dimension': {
                    'name': 'rows',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                    'length': 3,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': rows_metadata,
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {
                'structural_type': 'numpy.int64',
            },
        }])


if __name__ == '__main__':
    unittest.main()
