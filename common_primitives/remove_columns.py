import copy
import typing
import os

from d3m import container, exceptions, utils as d3m_utils
from d3m.metadata import base as metadata_base, hyperparams
from d3m.primitive_interfaces import base, transformer

import common_primitives

__all__ = ('RemoveColumnsPrimitive',)

Inputs = container.Dataset
Outputs = container.Dataset


class Hyperparams(hyperparams.Hyperparams):
    resource_id = hyperparams.Hyperparameter[typing.Union[str, None]](
        default=None,
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description='Resource ID of columns to remove if there are multiple resources inside a Dataset.'
    )
    columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description='A set of column indices to remove.'
    )


class RemoveColumnsPrimitive(transformer.TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
    """
    A primitive which removes columns from a DataFrame inside a Dataset. Columns are specified by index.
    """

    metadata = metadata_base.PrimitiveMetadata(
        {
            'id': '2eeff053-395a-497d-88db-7374c27812e6',
            'version': '0.2.0',
            'name': "Column remover",
            'python_path': 'd3m.primitives.datasets.RemoveColumns',
            'source': {
               'name': common_primitives.__author__,
            },
            'installation': [{
               'type': metadata_base.PrimitiveInstallationType.PIP,
               'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common_primitives'.format(
                   git_commit=d3m_utils.current_git_commit(os.path.dirname(__file__)),
               ),
            }],
            'algorithm_types': [
                metadata_base.PrimitiveAlgorithmType.ARRAY_SLICING,
            ],
            'primitive_family': metadata_base.PrimitiveFamily.DATA_TRANSFORMATION,
        },
    )

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> base.CallResult[Outputs]:
        resource_id = self.hyperparams['resource_id']

        if resource_id is None:
            if len(inputs) != 1:
                raise ValueError("A Dataset with multiple resources and no DataFrame resource specified as a hyper-parameter.")

            resource_id, resource = list(inputs.items())[0]

        else:
            resource = inputs[resource_id]

        keep_columns = self._get_columns(inputs.metadata, resource_id, self.hyperparams)

        outputs = copy.copy(inputs)
        # Just to make sure.
        outputs.metadata = inputs.metadata.set_for_value(outputs, generate_metadata=False, source=self)

        # Update data.
        resource = resource.iloc[:, keep_columns]
        outputs[resource_id] = resource

        # Update metadata.
        outputs.metadata = self._select_columns_metadata(outputs.metadata, resource_id, keep_columns, self)

        return base.CallResult(outputs)

    @classmethod
    def _get_columns(cls, inputs_metadata: metadata_base.DataMetadata, resource_id: str, hyperparams: Hyperparams) -> typing.Sequence[int]:
        all_columns = range(inputs_metadata.query((resource_id, metadata_base.ALL_ELEMENTS))['dimension']['length'])
        keep_columns = [column_index for column_index in all_columns if column_index not in hyperparams['columns']]
        return keep_columns

    @classmethod
    def _select_columns_metadata(cls, inputs_metadata: metadata_base.DataMetadata, resource_id: str, columns: typing.Sequence[int], source: typing.Any) -> metadata_base.DataMetadata:
        """
        This is similar to ``select_columns_metadata`` but operates on a Dataset.
        """

        if not columns:
            raise exceptions.InvalidArgumentValueError("No columns selected.")

        # This makes a copy.
        output_metadata = inputs_metadata.update(
            (resource_id, metadata_base.ALL_ELEMENTS,),
            {
                'dimension': {
                    'length': len(columns),
                },
            },
            source=source,
        )

        metadata_entry = output_metadata._current_metadata.elements[resource_id]

        # TODO: Do this better. This change is missing an entry in metadata log.
        elements = metadata_entry.all_elements.elements
        metadata_entry.all_elements.elements = {}
        for i, column_index in enumerate(columns):
            if column_index in elements:
                # If "column_index" is really numeric, we re-enumerate it.
                if isinstance(column_index, int):
                    metadata_entry.all_elements.elements[i] = elements[column_index]
                else:
                    metadata_entry.all_elements.elements[column_index] = elements[column_index]

        return output_metadata

    @classmethod
    def can_accept(cls, *, method_name: str, arguments: typing.Dict[str, typing.Union[metadata_base.Metadata, type]],
                   hyperparams: Hyperparams) -> typing.Optional[metadata_base.DataMetadata]:
        output_metadata = super().can_accept(method_name=method_name, arguments=arguments, hyperparams=hyperparams)

        # If structural types didn't match, don't bother.
        if output_metadata is None:
            return None

        if 'inputs' not in arguments:
            return output_metadata

        inputs_metadata = typing.cast(metadata_base.DataMetadata, arguments['inputs'])

        resource_id = hyperparams['resource_id']

        if resource_id is None:
            if inputs_metadata.query(())['dimension']['length'] != 1:
                raise ValueError("A Dataset with multiple resources and no DataFrame resource specified as a hyper-parameter.")

            # This can be also "ALL_ELEMENTS" and it will work out, but we prefer a direct resource ID,
            # if available (so we reverse the list and pick the second element, because the first is
            # "ALL_ELEMENTS" if it exists.
            resource_id = list(reversed(inputs_metadata.get_elements(())))[0]

        keep_columns = cls._get_columns(inputs_metadata, resource_id, hyperparams)

        return cls._select_columns_metadata(inputs_metadata, resource_id, keep_columns, cls)
