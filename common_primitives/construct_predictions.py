import os
import typing

from d3m import container, utils as d3m_utils
from d3m.metadata import base as metadata_base, hyperparams
from d3m.primitive_interfaces import base, transformer

import common_primitives
from common_primitives import utils

__all__ = ('ConstructPredictionsPrimitive',)

Inputs = container.DataFrame
Outputs = container.DataFrame


class Hyperparams(hyperparams.Hyperparams):
    use_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of column indices to force primitive to operate on. If any specified column is not a primary index or a predicted target, it is skipped.",
    )
    exclude_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of column indices to not operate on. Applicable only if \"use_columns\" is not provided.",
    )


class ConstructPredictionsPrimitive(transformer.TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
    """
    A primitive which takes as input a DataFrame and outputs a DataFrame in Lincoln Labs predictions
    format: first column a d3mIndex column (and other primary index columns, e.g., for object detection
    problem), and then predicted targets, each in its column.

    It supports both input columns annotated with semantic types (`https://metadata.datadrivendiscovery.org/types/PrimaryKey`,
    `https://metadata.datadrivendiscovery.org/types/PredictedTarget`), or trying to reconstruct metadata.
    This is why the primitive takes also additional input of a reference DataFrame which should
    have metadata to help reconstruct missing metadata. If metadata is missing, the primitive
    assumes that all ``inputs`` columns are predicted targets.
    """

    metadata = metadata_base.PrimitiveMetadata(
        {
            'id': '8d38b340-f83f-4877-baaa-162f8e551736',
            'version': '0.3.0',
            'name': "Construct pipeline predictions output",
            'python_path': 'd3m.primitives.data.ConstructPredictions',
            'source': {
               'name': common_primitives.__author__,
            },
            'installation': [{
               'type': metadata_base.PrimitiveInstallationType.PIP,
               'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common_primitives'.format(
                   git_commit=d3m_utils.current_git_commit(os.path.dirname(__file__)),
               ),
            }],
            'algorithm_types': [
                metadata_base.PrimitiveAlgorithmType.DATA_CONVERSION,
            ],
            'primitive_family': metadata_base.PrimitiveFamily.DATA_TRANSFORMATION,
        },
    )

    def produce(self, *, inputs: Inputs, reference: Inputs, timeout: float = None, iterations: int = None) -> base.CallResult[Outputs]:  # type: ignore
        index_column = utils.get_index_column(inputs.metadata)
        target_columns = utils.list_columns_with_semantic_types(inputs.metadata, ('https://metadata.datadrivendiscovery.org/types/PredictedTarget',))

        if index_column is not None and target_columns:
            return base.CallResult(self._produce_using_semantic_types(inputs, index_column, target_columns))
        else:
            return base.CallResult(self._produce_reconstruct(inputs, reference, index_column, target_columns))

    @classmethod
    def _get_columns(cls, index_column: int, target_columns: typing.Sequence[int], hyperparams: Hyperparams) -> typing.Sequence[int]:
        assert index_column is not None
        assert target_columns

        if hyperparams['use_columns']:
            if index_column not in hyperparams['use_columns']:
                raise ValueError("Index column not listed in \"use_columns\" hyper-parameter, but index column is required.")

            target_columns = [target_column_index for target_column_index in target_columns if target_column_index in hyperparams['use_columns']]
            if not target_columns:
                raise ValueError("No target columns listed in \"use_columns\" hyper-parameter, but target columns are required.")

        else:
            if index_column in hyperparams['exclude_columns']:
                raise ValueError("Index column listed in \"exclude_columns\" hyper-parameter, but index column is required.")

            target_columns = [target_column_index for target_column_index in target_columns if target_column_index not in hyperparams['exclude_columns']]
            if not target_columns:
                raise ValueError("All target columns listed in \"exclude_columns\" hyper-parameter, but target columns are required.")

        assert index_column is not None
        assert target_columns

        return [index_column] + target_columns

    def _produce_using_semantic_types(self, inputs: Inputs, index_column: int, target_columns: typing.Sequence[int]) -> Outputs:
        return utils.select_columns(inputs, self._get_columns(index_column, target_columns, self.hyperparams), source=self)

    @classmethod
    def _produce_using_semantic_types_metadata(cls, inputs_metadata: metadata_base.DataMetadata, index_column: int,
                                               target_columns: typing.Sequence[int], hyperparams: Hyperparams) -> metadata_base.DataMetadata:
        return utils.select_columns_metadata(inputs_metadata, cls._get_columns(index_column, target_columns, hyperparams), source=cls)

    def _produce_reconstruct(self, inputs: Inputs, reference: Inputs, index_column: typing.Optional[int], target_columns: typing.Sequence[int]) -> Outputs:
        if index_column is None:
            reference_index_column = utils.get_index_column(reference.metadata)

            if reference_index_column is None:
                raise ValueError("Cannot find an index column in reference data, but index column is required.")

            index = utils.select_columns(reference, [reference_index_column], source=self)
        else:
            index = utils.select_columns(inputs, [index_column], source=self)

        if not target_columns:
            if index_column is not None:
                raise ValueError("No target columns in input data, but index column present.")

            # We assume all inputs are targets.
            targets = inputs

            # We make sure at least basic metadata is generated correctly, so we regenerate metadata.
            targets.metadata = targets.metadata.set_for_value(targets, generate_metadata=True, source=self)

            # We set target column names from the reference.
            targets.metadata = self._set_target_names(targets.metadata, self._get_target_names(reference.metadata), self)

        else:
            targets = utils.select_columns(inputs, target_columns, source=self)

        return utils.append_columns(index, targets, source=self)

    @classmethod
    def _produce_reconstruct_metadata(cls, inputs_metadata: metadata_base.DataMetadata, reference_metadata: metadata_base.DataMetadata,
                                      index_column: typing.Optional[int], target_columns: typing.Sequence[int]) -> metadata_base.DataMetadata:
        if index_column is None:
            reference_index_column = utils.get_index_column(reference_metadata)

            if reference_index_column is None:
                raise ValueError("Cannot find an index column in reference data, but index column is required.")

            index_metadata = utils.select_columns_metadata(reference_metadata, [reference_index_column], source=cls)
        else:
            index_metadata = utils.select_columns_metadata(inputs_metadata, [index_column], source=cls)

        if not target_columns:
            if index_column is not None:
                raise ValueError("No target columns in input data, but index column present.")

            # We assume all inputs are targets.
            targets_metadata = inputs_metadata

            # We set target column names from the reference.
            targets_metadata = cls._set_target_names(targets_metadata, cls._get_target_names(reference_metadata), cls)

        else:
            targets_metadata = utils.select_columns_metadata(inputs_metadata, target_columns, source=cls)

        return utils.append_columns_metadata(index_metadata, targets_metadata, source=cls)

    def multi_produce(self, *, produce_methods: typing.Sequence[str], inputs: Inputs, reference: Inputs, timeout: float = None, iterations: int = None) -> base.MultiCallResult:  # type: ignore
        return self._multi_produce(produce_methods=produce_methods, timeout=timeout, iterations=iterations, inputs=inputs, reference=reference)

    @classmethod
    def _get_target_names(cls, metadata: metadata_base.DataMetadata) -> typing.List[typing.Union[str, None]]:
        target_names = []

        for column_index in utils.list_columns_with_semantic_types(metadata, ('https://metadata.datadrivendiscovery.org/types/TrueTarget',)):
            column_metadata = metadata.query((metadata_base.ALL_ELEMENTS, column_index))

            target_names.append(column_metadata.get('name', None))

        return target_names

    @classmethod
    def _set_target_names(cls, metadata: metadata_base.DataMetadata, target_names: typing.Sequence[typing.Union[str, None]], source: typing.Any) -> metadata_base.DataMetadata:
        targets_length = metadata.query((metadata_base.ALL_ELEMENTS,))['dimension']['length']

        if targets_length != len(target_names):
            raise ValueError("Not an expected number of target columns to apply names for. Expected {target_names}, provided {targets_length}.".format(
                target_names=len(target_names),
                targets_length=targets_length,
            ))

        for column_index, target_name in enumerate(target_names):
            # We do not have it, let's skip it and hope for the best.
            if target_name is None:
                continue

            metadata = metadata.update_column(column_index, {
                'name': target_name,
            }, source=source)

        return metadata

    @classmethod
    def can_accept(cls, *, method_name: str, arguments: typing.Dict[str, typing.Union[metadata_base.Metadata, type]], hyperparams: Hyperparams) -> typing.Optional[metadata_base.DataMetadata]:
        output_metadata = super().can_accept(method_name=method_name, arguments=arguments, hyperparams=hyperparams)

        # If structural types didn't match, don't bother.
        if output_metadata is None:
            return None

        if 'inputs' not in arguments or 'reference' not in arguments:
            return output_metadata

        inputs_metadata = typing.cast(metadata_base.DataMetadata, arguments['inputs'])
        reference_metadata = typing.cast(metadata_base.DataMetadata, arguments['reference'])

        index_column = utils.get_index_column(inputs_metadata)
        target_columns = utils.list_columns_with_semantic_types(inputs_metadata, ('https://metadata.datadrivendiscovery.org/types/PredictedTarget',))

        if index_column is not None and target_columns:
            return cls._produce_using_semantic_types_metadata(inputs_metadata, index_column, target_columns, hyperparams)
        else:
            return cls._produce_reconstruct_metadata(inputs_metadata, reference_metadata, index_column, target_columns)
